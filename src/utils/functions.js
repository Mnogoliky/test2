/**
 *
 * @param {Object} obj - передаваемые данные в виде объекта
 * @returns {string} - Сгенерированное тело запроса в строке
 */
export const objectToBody = obj => Object.keys(obj).map((key) => `${key}=${obj[key]}`).join('&');

/**
 *
 * @param {string} type
 * @param {function} funBody
 * @return {function(...[*]): {type: object}}
 */
export const createAction = (type, funBody) => {
	const newFun = (...props) => ({type, ...funBody(...props)});
	newFun.toString = () => type;
	return newFun;
};

/**
 *
 * @param {number | string | Date} innerdate
 * @return {string}
 */
export const getTimeFromDate = innerdate => {
	const date = new Date(innerdate);

	return `${ date.getHours() }:${ date.getMinutes().toString().padStart(2, '0') }`;
};

/**
 * @param {number | string | Date} innerDate
 * @return {string} date
 */
export const getDateStr = (innerDate) => {
	const date = new Date(innerDate);
	const monthes = [
		'January',
		'February',
		'March',
		'April',
		'May',
		'June',
		'July',
		'August',
		'September',
		'October',
		'November',
		'December',
	];
	return `${date.getDate()} ${monthes[date.getMonth()]} ${date.getFullYear()}`;
};