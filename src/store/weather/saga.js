import { all, takeEvery, put } from 'redux-saga/effects';
import * as api from './api';
import * as actions from './actions';

function* getWeather() {
	try {
		yield put(actions.setLoadStatus(true));
		const response = yield api.getWeather();
		if (response.ok)
		{
			const data = yield response.json();
			yield put(actions.setWeatherData(data));
		}
		else
			yield put(actions.setError({message: response.statusMessage}));
		yield put(actions.setLoadStatus(false));
	}
	catch (e) {
		yield put(actions.setLoadStatus(false));
		yield put(actions.setError(e));
	}
}

export function* watchAllWeather() {
	yield all([
		takeEvery(actions.getWeatherData.toString(), getWeather),
	]);
}
