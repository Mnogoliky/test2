import { objectToBody } from '../../utils';

const requestConfig = {
	q: 'Kharkiv,ua',
	units: 'metric&mode=json',
	appid: process.env.REACT_APP_API_KEY,
};

export const getWeather = () => fetch(`https://api.openweathermap.org/data/2.5/forecast?${objectToBody(requestConfig)}`);