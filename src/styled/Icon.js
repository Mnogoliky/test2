import styled from 'styled-components';
import PropTypes from 'prop-types'

export const Icon = styled.img`
	width: ${p => p.size || 'auto'};
	height: ${p => p.size || 'auto'};
	margin: auto 0
`;

Icon.propTypes = {
	size: PropTypes.number,
};