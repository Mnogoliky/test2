import styled, { keyframes } from 'styled-components';

const rotate = keyframes`
  from {
    transform: rotate(0deg);
  }

  to {
    transform: rotate(360deg);
  }
`;

export const Loader = styled.div`
	width: 32px;
	height: 32px;
	border-radius: 100%
	border: 2px solid black;
	border-top-color: transparent;
	
	animation: ${rotate} 1s linear infinite;
`;