import styled from 'styled-components'
import PropTypes from 'prop-types'

export const Separator = styled.div`
	flex-direction: ${p => p.horizontally ? 'column' : 'row'};
	display: flex;
	justify-content: space-between;
	width: ${p => p.width || 'auto'}
	height: ${p => p.height || 'auto'}
`;

Separator.propTypes = {
	horizontally: PropTypes.bool,
	width: PropTypes.string,
	height: PropTypes.string,
};