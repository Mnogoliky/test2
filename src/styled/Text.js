import styled from 'styled-components';
import PropTypes from 'prop-types';

export const Text = styled.p`
	font-family: sans-serif;
	margin: 10px 0;
	font-weight: 700;
	font-size: ${p => p.size || 'auto' }
`;

Text.PropTypes = {
	size: PropTypes.string,
};