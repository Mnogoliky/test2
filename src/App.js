import React, { useEffect } from 'react';
import { connect } from 'react-redux'
import * as actions from './store/weather/actions';

import { Dimmer, Separator } from './styled';
import { BigViget } from './components/BigViget';
import { LittleViget } from './components/LittleViget';

const App = ({dispatch}) => {

	useEffect(() => {
		dispatch(actions.getWeatherData())
	});

	const days = [0, 1, 2, 3, 4, 5];

	return (
		<div className = "App">
			<Dimmer>
				<Separator horizontally>
					<BigViget/>
					<Separator>
						{
							days.map((val, i) => <LittleViget key = { i } dayIncrement = { val }/>)
						}
					</Separator>
				</Separator>
			</Dimmer>
		</div>
	);
};

export default connect()(App);
