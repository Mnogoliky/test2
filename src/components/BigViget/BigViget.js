import React from 'react';
import { connect } from 'react-redux';
import { Box, Separator, Dimmer, Loader, Text, Icon } from '../../styled';
import { getTimeFromDate, getDateStr } from '../../utils';

const BigVigetContainer = ({isLoad, error, data}) => {

	const {weather, main} = data.list[0];
	const {city} = data;

	return (
		<Box height = '145px'>
			{
				isLoad ?
					(
						<Dimmer>
							<Loader/>
						</Dimmer>
					) :
					error ? (
							<Dimmer>
								<Text>Something went wrong</Text>
							</Dimmer>
						) :
						(
							<Separator height = '100%'>
								<Separator horizontally>
									<div>
										<Text size = '13px'>{ getDateStr(new Date) }</Text>
										<Text size = '20px'>{ weather[0].description }</Text>
									</div>
									<div>
										<Separator>
											<div>
												<Text size = '25px'>{ main.temp }°C</Text>
											</div>
											<Icon
												size = '45px'
												src = { `https://openweathermap.org/img/wn/${weather[0].icon}@2x.png` }
												alt = ""
											/>
										</Separator>
									</div>
								</Separator>
								<div>
									<Text size = '13px'>Pressure: { main.pressure } hpa</Text>
									<Text size = '13px'>Humidity: { main.humidity } %</Text>

									<Text size = '13px'>Sunrise: { getTimeFromDate(city.sunrise) }</Text>
									<Text size = '13px'>Sunset: { getTimeFromDate(city.sunset) }</Text>
								</div>
							</Separator>
						)
			}
		</Box>
	)
};

export const BigViget = connect(state => ({
	isLoad: state.weatherReducer.isLoad,
	data: state.weatherReducer.data,
	error: state.weatherReducer.error,
}))(BigVigetContainer);