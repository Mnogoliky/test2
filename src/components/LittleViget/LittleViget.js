import React from 'react';
import { connect } from 'react-redux';
import { Box, Separator, Dimmer, Loader, Text, Icon } from '../../styled';
import { getDateStr } from '../../utils';
import PropTypes from 'prop-types';

const LittleVigetContainer = ({isLoad, error, dayIncrement, data}) => {

	const mockdata = {weather: [{icon: ''}], main: {}};
	const {weather, main} = data.list[dayIncrement] || mockdata;

	return (
		<Box width = '145px' height = '125px'>
			{
				isLoad ?
					(
						<Dimmer>
							<Loader/>
						</Dimmer>
					) :
					error ? (
							<Dimmer>
								<Text>Something went wrong</Text>
							</Dimmer>
						) :
						(
							<Separator horizontally>
								<Text size = '13px'>{ getDateStr(+(new Date) + (86400000 * dayIncrement)) }</Text>
								<div>
									<Separator>
										<div>
											<Text size = '20px'>{ main.temp }°C</Text>
										</div>
										<Icon size = '30px' src = { `https://openweathermap.org/img/wn/${weather[0].icon}@2x.png` }
													alt = ""/>
									</Separator>
								</div>
							</Separator>
						)
			}
		</Box>
	)
};

export const LittleViget = connect(state => ({
	isLoad: state.weatherReducer.isLoad,
	data: state.weatherReducer.data,
	error: state.weatherReducer.error,
}))(LittleVigetContainer);

LittleViget.defaultProps = {
	dayIncrement: 0,
};

LittleViget.PropTypes = {
	dayIncrement: PropTypes.number,
};